import hashlib
from pathlib import Path
from configparser import ConfigParser

def md5(context):
    return hashlib.md5(context).hexdigest()


def str2md5(s):
    return md5(str(s).encode())

class Config:
    configPath: Path
    cf: ConfigParser

    def __init__(self, configPath: Path | str):
        self.configPath = configPath
        self.cf = ConfigParser()
        if not self.cf.read(configPath, encoding="utf-8"):
            raise FileNotFoundError("CounterSide is not Founded or Config.ini not founded")

    def get(self, section: str, option: str, raw=False) -> str:
        return self.cf.get(section, option, raw=raw)

    def getint(self, section: str, option: str, raw=False) -> int:
        return self.cf.getint(section, option, raw=raw)

    def getboolean(self, section: str, option: str, raw=False) -> bool:
        return self.cf.getboolean(section, option, raw=raw)

    def items(self, section: str, raw=False):
        return self.cf.items(section, raw=raw)
    
    def set(self, section: str, option: str, value: str):
        if not self.cf.has_section(section):
            self.cf.add_section(section)
        self.cf.set(section, option, value)
        with self.configPath.open("w") as f:
            self.cf.write(f)