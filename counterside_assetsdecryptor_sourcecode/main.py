import io
import os

# import lz4.block
# import UnityPy
from progress.bar import IncrementalBar
from pathlib import Path
from utils import *
from NumericExt import *


class InitBar(IncrementalBar):
    suffix_msg = ""
    suffix = "%(percent).2f%% %(index)d/%(max)d %(suffix_msg)s"


# from UnityPy.helpers import TypeTreeHelper
# TypeTreeHelper.read_typetree_c = False


config = Config("config.ini")

# 游戏目录
CounterSidePath = Path(config.get("game", "path"))

# 解密目录
OutputPath = Path(config.get("game", "output"))


StreamingAssetsPath = CounterSidePath / "Data" / "StreamingAssets"

Decrypt2MaskList = [
    14003937370121879411,
    295159725236528685,
    14656252856989855980,
    3126201044280739051,
    6176412274767465921,
    8501111619623644353,
    1001882303165547266,
    889784367385610816,
    8403001398375820177,
    15646421979254498160,
    15540104736269140030,
    4473111575030559303,
    16641115610173278858,
    7005653296469604124,
    7641466651897675454,
    18242667629599333687,
]


def decrypt2(data, offset=0):
    num = 0
    num2 = 0
    size = len(data)
    buffer = bytearray(size)
    while num2 < size:
        num3 = Decrypt2MaskList[num]
        num4 = offset + num2
        num5 = size - num2
        if num5 >= 8:
            num6 = DirectToUint64(data, num4)
            num6 = (
                (num6 & 0xFFFFFFFF00000000)
                | ((num6 & 0xFF000000) >> 8)
                | ((num6 & 0xFF0000) << 8)
                | ((num6 & 0xFF00) >> 8)
                | ((num6 & 0xFF) << 8)
            )
            num7 = num6 & 0x5555555555555555
            num6 = ((num6 & 0xAAAAAAAAAAAAAAAA) >> 1) | (num7 << 1)

            __data = num6 ^ num3
            buffer[num4] = __data & 0xFF
            buffer[num4 + 1] = (__data >> 8) & 0xFF
            buffer[num4 + 2] = (__data >> 16) & 0xFF
            buffer[num4 + 3] = (__data >> 24) & 0xFF
            buffer[num4 + 4] = (__data >> 32) & 0xFF
            buffer[num4 + 5] = (__data >> 40) & 0xFF
            buffer[num4 + 6] = (__data >> 48) & 0xFF
            buffer[num4 + 7] = (__data >> 56) & 0xFF
            num2 += 8
        else:
            for i in range(num5):
                buffer[num4 + i] ^= num3 & (255 << i) >> i
            num2 += num5
        num = (num + 1) % len(Decrypt2MaskList)

    return bytes(buffer)


# def decompress_lz4(data: bytes, uncompressed_size: int) -> bytes:
#     print("重写")
#     try:
#         ret = lz4.block.decompress(data, uncompressed_size)
#     except lz4.block.LZ4BlockError:
#         ret = lz4.block.decompress(decrypt2(data), uncompressed_size)


#     return ret


# UnityPy.helpers.CompressionHelper.decompress_lz4 = decompress_lz4


def GetMaskList(path):
    name_md5 = str2md5(path.stem)
    s = name_md5[0:16]
    s2 = name_md5[16 : 16 * 2]
    s3 = name_md5[0:8] + name_md5[16 : 16 + 8]
    s4 = name_md5[8 : 8 * 2] + name_md5[24 : 24 + 8]
    return [int(s, 16), int(s2, 16), int(s3, 16), int(s4, 16)]


def decrypt(SAPath: Path, Output: Path):
    file_list_len = len(os.listdir(SAPath))
    bar = InitBar("正在解密", max=file_list_len)

    for AssetFile in SAPath.iterdir():
        if AssetFile.is_dir():
            (Output / AssetFile.name).mkdir(parents=True, exist_ok=True)
            decrypt(AssetFile, Output / AssetFile.name)
            continue
        if AssetFile.suffix in [".mp4"]:
            continue

        # print("正在解密 %s" % AssetFile.name)

        maskList = GetMaskList(AssetFile)
        maskIndex = 0
        with AssetFile.open("rb") as f:
            data = f.read()
            size = min(len(data), 212)

            buffer = bytearray(size)

            num = 0
            while num < size:
                num2 = maskList[maskIndex]
                num3 = size - num
                if num3 >= 8:
                    __data = DirectToUint64(data, num) ^ num2
                    buffer[num] = __data & 0xFF
                    buffer[num + 1] = (__data >> 8) & 0xFF
                    buffer[num + 2] = (__data >> 16) & 0xFF
                    buffer[num + 3] = (__data >> 24) & 0xFF
                    buffer[num + 4] = (__data >> 32) & 0xFF
                    buffer[num + 5] = (__data >> 40) & 0xFF
                    buffer[num + 6] = (__data >> 48) & 0xFF
                    buffer[num + 7] = (__data >> 56) & 0xFF
                    num += 8
                else:
                    for i in range(num, size):
                        num4 = i - num
                        buffer[i] = data[i] ^ (num2 & ((255 << num4) >> num4))
                    num += num3

                maskIndex = (maskIndex + 1) % len(maskList)

            with (Output / AssetFile.name).open("wb") as f:
                f.write(buffer + data[size:])
                # print(f"解密完成: {AssetFile.name}")
                bar.suffix_msg = "解密完成-> %s" % AssetFile.name
                bar.next()
    bar.finish()


if __name__ == "__main__":
    decrypt(StreamingAssetsPath, OutputPath)
    os.system("pause")
