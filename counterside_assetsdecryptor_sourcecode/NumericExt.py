def DirectToUint64(buffer, startIndex):
    return (
        (
            (
                (
                    (
                        (
                            (
                                (
                                    (
                                        (
                                            (
                                                (
                                                    (buffer[startIndex + 7] << 8)
                                                    | buffer[startIndex + 6]
                                                )
                                                << 8
                                            )
                                            | buffer[startIndex + 5]
                                        )
                                        << 8
                                    )
                                    | buffer[startIndex + 4]
                                )
                                << 8
                            )
                            | buffer[startIndex + 3]
                        )
                        << 8
                    )
                    | buffer[startIndex + 2]
                )
                << 8
            )
            | buffer[startIndex + 1]
        )
        << 8
    ) | buffer[startIndex]
